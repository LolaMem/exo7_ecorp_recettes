
Use Stocks;

CREATE TABLE recettes (
  recette_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  recette VARCHAR(50)
  );

CREATE TABLE recette_ingredient (
  recette_id INT NOT NULL REFERENCES recettes(recette_id),
  ingredient_id INT NOT NULL REFERENCES product(product_id),
  quantity INT,
  PRIMARY KEY (recette_id, ingredient_id)
  );


