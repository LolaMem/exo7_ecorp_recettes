

import pandas as pd
from norm import wow
import mysql.connector

# Read the csv and normalized the header
df = pd.read_csv('extract_B.csv')
df.columns = ["product","date", "stock"]

# Clean data: eliminate plurals, "ligatures" and accents
df["product"] = wow(df["product"])

# Find duplicates and keep only stock from latest date
df['date'] = pd.to_datetime(df['date'], format='%d/%m/%Y')
# Sort the DataFrame by the 'date' column in descending order
df = df.sort_values(by='date', ascending=False)
# Keep only the first occurrence (latest date) for each 'product'
df = df.drop_duplicates(subset='product', keep='first')
# Eliminate column date
df = df.drop(columns=['date'])

# Sort by product and reset the index
df = df.sort_values(by="product")
df = df.reset_index(drop=True)

# Sort the DataFrame
final_df = df.sort_values(by='product')

# Negatif values set to 0
final_df['stock'] = final_df['stock'].apply(lambda x: max(0, x))

# Add column to identify the restaurant
#final_df["restaurant"] = 2

#print(final_df)

###################################""""
mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    auth_plugin="mysql_native_password",  # Use the correct authentication plugin
    database='Stocks'
)

cursor = mydb.cursor()

# Insert values into restaurant table
cursor.execute("INSERT INTO restaurant VALUES (NULL, 'rest_B');")
resto_id = cursor.lastrowid
print(resto_id)
mydb.commit()


# Insert not repeated vaslues into product table and all values into stock table

for index, row in final_df.iterrows():
    product = row['product'].replace("'", "\'")

    # After executing load_A.py, verify that the products to introduce aren't already in the DB
    cursor.execute("SELECT product_id FROM product WHERE product = %s;", (product,))
    result = cursor.fetchone()

    # If the product doesn't exist in product table, it is added
    if not result:
        cursor.execute("INSERT INTO product (product) VALUES (%s);", (product,))
        product_id = cursor.lastrowid
        print(product_id, product)

     # Insert all lines into stock table
    else:
        product_id = result[0]
    cursor.execute("INSERT INTO stock (product_id, product, stock_qty, resto_id) VALUES (%s, %s, %s, %s)", (product_id, product, row['stock'], resto_id))

mydb.commit()
cursor.close()
mydb.close()