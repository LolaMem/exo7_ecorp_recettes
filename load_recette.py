
import pandas as pd
from norm import wow
import re
import mysql.connector

# Read the csv and normalized the header
df = pd.read_csv('recettes.csv')
df.columns = ["ingredient","quantity", "recette"]

# Clean data: eliminate plurals, "ligatures" and accents
df["ingredient"] = wow(df["ingredient"])

# Remove both double and single quotes, convert to lowercase, and strip leading/trailing spaces 
df['recette'] = df['recette'].apply(lambda x: re.sub(r'["\']', '', x).lower().strip())

# # Create a new DataFrame with unique recipes after stripping leading/trailing spaces
# df_recettes = df['recette'].str.strip().drop_duplicates()
# print(df_recettes)

print(df)

# # ###################################""""
mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    auth_plugin="mysql_native_password",  # Use the correct authentication plugin
    database='Stocks'
)

cursor = mydb.cursor()

# One loop to insert values into recettes, product and recette_ingredient tables
for index, row in df.iterrows():
    # Insert values into recettes table after verifying they don't exist already
    recette = row['recette'].replace("'", "\'")
    cursor.execute("SELECT recette_id FROM recettes WHERE recette = %s", (recette,))
    result = cursor.fetchone()

    if result:
        recette_id = result[0]
        #print(recette_id, row["recette"])

    else:
        cursor.execute("INSERT IGNORE INTO recettes (recette) VALUES (%s)", (recette,))
        mydb.commit()
        recette_id = cursor.lastrowid

    # Consume any unread result
    cursor.fetchall()

    # Insert values into product table after verifying they don't exist already
    ingredient = row['ingredient'].replace("'", "\'")

    cursor.execute("SELECT product_id FROM product WHERE (product) = (%s);", (ingredient,))
    result = cursor.fetchone()

    if not result:
        cursor.execute("INSERT INTO product (product_id, product) VALUES (NULL, %s);", (ingredient,))
        product_id = cursor.lastrowid
    else:
        product_id = result[0]
        print(result)

    # Insert values into recette_ingredient table with the quantity
    cursor.execute("INSERT INTO recette_ingredient (recette_id, ingredient_id, quantity) VALUES (%s, %s, %s);", (recette_id, product_id, row['quantity']))

    # Consume any unread result
    cursor.fetchall()

# Valider les changements
mydb.commit()




# # Insert values into recette table
# for index, row in df_recettes.iterrows():
#     # After executing load_A.py, verify that the products to introduce aren't already in the DB
#     cursor.execute("INSERT INTO recettes VALUES (NULL,%s);", (row["recette"],))
#     result = cursor.fetchone()

#     # If the product doesn't exist in product table, it is added
#     if not result:
#         cursor.execute("INSERT INTO product (product) VALUES (%s);", (product,))
#         product_id = cursor.lastrowid
#         print(product_id, product)

#      # Insert all lines into stock table
#     else:
#         product_id = result[0]
#     cursor.execute("INSERT INTO stock (product_id, product, stock_qty, resto_id) VALUES (%s, %s, %s, %s)", (product_id, product, row['stock'], resto_id))

# mydb.commit()

# cursor.close()
# mydb.close()
