DROP DATABASE IF EXISTS Stocks;

CREATE DATABASE Stocks;
Use Stocks;

CREATE TABLE restaurant (
  resto_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  restaurant VARCHAR(10)
  );

CREATE TABLE product (
  product_id INT PRIMARY KEY AUTO_INCREMENT,
  product VARCHAR(50)
  );

CREATE TABLE stock (
  product_id INT REFERENCES product(product_id),
  product VARCHAR(30),
  stock_qty INT,
  resto_id INT REFERENCES restaurant(resto_id),
  PRIMARY KEY (product_id, resto_id)
  );

DROP USER IF EXISTS lolam1@localhost;
CREATE USER lolam1@localhost IDENTIFIED BY 'lolam1';
GRANT ALL PRIVILEGES ON Stocks.* TO 'lolam1'@'localhost';
