
import pandas as pd
from norm import wow
import mysql.connector


# Read the csv and normalized the header
df = pd.read_csv('extract_A.csv')

# Clean data: eliminate plurals, "ligatures" and accents
df["product"] = wow(df["product"])

# Group by 'product' and sum 'stock'
df1 = df.groupby('product')['stock'].sum().reset_index()

# Sort the DataFrame
final_df = df1.sort_values(by='product')

# Negatif values set to 0
final_df['stock'] = final_df['stock'].apply(lambda x: max(0, x))

# Add column to identify the restaurant
#final_df["restaurant"] = 1

#print(final_df)

# Connect with mysql.connector and create a cursor to execute the requests into SQL DDBB

mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    auth_plugin="mysql_native_password",  # Use the correct authentication plugin
    database='Stocks'
)

cursor = mydb.cursor()

# Insert values into restaurant table
cursor.execute("INSERT INTO restaurant VALUES (NULL, 'rest_A');")
resto_id = cursor.lastrowid
print(resto_id)
mydb.commit()


# Insert values into product and stock tables
for index, row in final_df.iterrows():
    #print(row)
    product = row['product'].replace("'", "\'")
    cursor.execute("INSERT INTO product (product_id, product) VALUES (NULL, %s);", [product])
    product_id = cursor.lastrowid
    print(product_id, product)
    mydb.commit()
    cursor.execute("INSERT INTO stock (product_id, product, stock_qty, resto_id) VALUES (%s, %s, %s, %s)", (product_id, product, row['stock'], resto_id))

mydb.commit()

cursor.close()
mydb.close()

