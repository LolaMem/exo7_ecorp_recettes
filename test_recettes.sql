
USE Stocks;

-- SELECT 'Create 1 new recette smoothie d'orange: lait 5, orange 1, cannelle poudre 2, banana 2';
INSERT IGNORE INTO recettes VALUES (NULL, 'smoothie d\'orange');
SET @id_smoothie = LAST_INSERT_ID();

-- If the recette already exists, retrieve the existing recette_id
SELECT COALESCE((SELECT recette_id FROM recettes WHERE recette = 'smoothie d\'orange' LIMIT 1), @id_smoothie) 
INTO @id_smoothie;

SELECT * FROM recettes;

-- Insert all necessary ingredients in table product and drop duplicates
INSERT INTO product (product) VALUES 
  ('lait'), 
  ('orange'),
  ('cannelle poudre'), 
  ('banana')
  ON DUPLICATE KEY UPDATE product = VALUES(product);


SELECT * FROM product;

INSERT INTO recette_ingredient VALUES 
(@id_smoothie, (select product_id from product WHERE product  LIKE 'lait' LIMIT 1), 5),
(@id_smoothie, (select product_id from product WHERE product  LIKE 'orange' LIMIT 1), 1),
(@id_smoothie, (select product_id from product WHERE product  LIKE 'cannelle poudre' LIMIT 1), 2),
(@id_smoothie, (select product_id from product WHERE product  LIKE 'banana' LIMIT 1), 2);


SELECT * FROM recette_ingredient;

