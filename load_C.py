
import pandas as pd
from norm import wow
import mysql.connector

# Read the csv and normalized the header
header_columns =["product", "stock"]
df = pd.read_csv('extract_C.csv', header=None, names= header_columns)

# Clean data: eliminate plurals, "ligatures" and accents
df["product"] = wow(df["product"])

# Group by 'product' and sum 'stock'
df1 = df.groupby('product')['stock'].sum().reset_index()

# Sort the DataFrame
final_df = df1.sort_values(by='product')

# Negatif values set to 0
final_df['stock'] = final_df['stock'].apply(lambda x: max(0, x))

# Add column to identify the restaurant
#final_df["restaurant"] = 3

#print(final_df)

# Connect with mysql.connector and create a cursor to execute the requests into SQL DDBB

mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    auth_plugin="mysql_native_password",  # Use the correct authentication plugin
    database='Stocks'
)

cursor = mydb.cursor()

# Insert values into restaurant table
cursor.execute("INSERT INTO restaurant VALUES (NULL, 'rest_C');")
resto_id = cursor.lastrowid
print(resto_id)
mydb.commit()


# Insert not repeated vaslues into product table and all values into stock table

for index, row in final_df.iterrows():
    product = row['product'].replace("'", "\'")

    # After executing load_A.py, verify that the products to introduce aren't already in the DB
    cursor.execute("SELECT product_id FROM product WHERE product = %s;", (product,))
    result = cursor.fetchone()

    # If the product doesn't exist in product table, it is added
    if not result:
        cursor.execute("INSERT INTO product (product) VALUES (%s);", (product,))
        product_id = cursor.lastrowid
        print(product_id, product)

     # Insert all lines into stock table
    else:
        product_id = result[0]
    cursor.execute("INSERT INTO stock (product_id, product, stock_qty, resto_id) VALUES (%s, %s, %s, %s)", (product_id, product, row['stock'], resto_id))

mydb.commit()

cursor.close()
mydb.close()