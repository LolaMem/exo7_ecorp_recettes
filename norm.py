import pandas as pd
from unidecode import unidecode
import re


def clean_data(df, id_resto):

    # Change to string data type
    df["product"] = df["product"].apply(str)  
    # Replace special characters
    df["product"] = df["product"].str.replace("œ", "oe")
    df["product"] = df["product"].str.replace("oeufs", "oeuf")
    #Change to upper case
    df["product"] = df["product"].apply(lambda x: x.upper())   
    # Normalize and remove accents
    df["product"] = \
    df["product"].str.normalize('NFKD').str.encode('ascii',errors='ignore').str.decode('utf-8')
    # Group by 'product' and sum 'stock'
    df1 = df.groupby('product')['stock'].sum().reset_index()
    # Sort the DataFrame
    final_df = df1.sort_values(by='product')

    final_df["restaurant"] = id_resto
    
    return final_df


def wow(list_original):
    return [unidecode(" ".join([t[:-1] if t.endswith("s") else t for t in exp.lower().split()])) for exp in list_original]

# if __name__ == '__main__':
#     df = pd.read_csv('extract_A.csv')
#     df["restaurant"] = 1
#     print(df)
#     df_A = clean_data(df, 1)
#     print(df_A)